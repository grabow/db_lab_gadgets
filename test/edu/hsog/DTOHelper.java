package edu.hsog;

import java.util.UUID;

public class DTOHelper {
    
    public static DTO generateDTO(String email) {
        DTO dto = new DTO();
        dto.email = email;
        dto.cover = null;
        dto.beschreibung = "Beschreibung";
        dto.userKommentar = "toll";
        dto.userGefallen = 8;
        dto.url = UUID.randomUUID().toString();

        return dto;
    }
    
    public static DTO generateDTO(String email, String url) {
        DTO dto = new DTO();
        dto.email = email;
        dto.cover = null;
        dto.beschreibung = "Beschreibung";
        dto.userKommentar = "toll";
        dto.userGefallen = 8;
        dto.url = url;

        return dto;
    }
}
