package edu.hsog;

import static junit.framework.Assert.assertEquals;
import org.junit.*;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import org.hsof.Helper;

public class GadgetDBTests {
    
    public static String[] tables = new String[]{"BEWERTUNG", "GADGETS", "USERS"};
    
    public static String WRONG_EMAIL = "W@web.de";
    public static String WRONG_PASSWORD = "W12345";
    
    public static String WRONG_URL = "url.de";
    
    public static String url = "jdbc:oracle:thin:@//localhost:1521/xe";
    public static Connection v_connection;
    
    @BeforeClass
    public static void initConnection() {
        Globals.initConnectionPool();
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            v_connection = DriverManager.getConnection(url,
                    "CHRIS",
                    "xyz"
            );
        } catch (ClassNotFoundException | SQLException e) {
            Logger.getLogger(GadgetDBTests.class.getName()).log(Level.SEVERE, null, e);
        }
        
        saveAndDeleteDb();
    }
    
    public static void saveAndDeleteDb() {
        Arrays.stream(tables).forEach(t -> Helper.tableToCSV(t, v_connection));
        deleteDb();
    }
    
    public static void deleteDb() {
        Arrays.stream(tables).forEach(t -> Helper.deleteTable(t, v_connection));
    }
    
    @After
    public void deleteDbAfterTest() {
        DBQueries.logIn = false;
        Arrays.stream(tables).forEach(t -> Helper.deleteTable(t, v_connection));
    }
    
    
    @AfterClass
    public static void closeConnection() {
        deleteDb();
        restoreDb();
        try {
            v_connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(GadgetDBTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void restoreDb() {
        Helper.CsvUSERSToDb(v_connection);
        Helper.CsvGADGETSToDb(v_connection);
        Helper.CsvBEWERTUNGToDb(v_connection);
    }

    @Test
    public void COUNT_SHOULD_RETURN_ZERO_WHEN_NO_DATA_IS_PRESENT() {
        assertEquals(DBQueries.count(), 0);
    }
    
    @Test
    public void COUNT_SHOULD_RETURN_CORRECT_NUMBER_WHEN_DATA_IS_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> Helper.insertDataToGadgets(v_connection, loginData[0]));
        assertEquals(count, DBQueries.count());
    }
    
    @Test
    public void LOGIN_SHOULD_RETURN_FALSE_WHEN_DB_IS_EMPTY() {
        assertEquals(DBQueries.login(WRONG_EMAIL, WRONG_PASSWORD), false);
    }
    
    @Test
    public void LOGIN_SHOULD_RETURN_FALSE_WHEN_EMAIL_IS_NOT_FOUND() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.login(WRONG_EMAIL, WRONG_PASSWORD), false);
    }
    
    @Test
    public void LOGIN_SHOULD_RETURN_FALSE_WHEN_PASSWORD_IS_WRONG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.login(loginData[0], WRONG_PASSWORD), false);
    }
    
    @Test
    public void LOGIN_SHOULD_RETURN_TRUE_WHEN_EMAIL_AND_PASSWORD_ARE_CORRECT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.login(loginData[0], loginData[1]), true);
    }
    
    @Test
    public void REGISTER_SHOULD_RETURN_TRUE_WHEN_DB_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        assertEquals(DBQueries.register(loginData[0], loginData[1]), true);
    }
    
    @Test
    public void REGISTER_SHOULD_RETURN_TRUE_WHEN_EMAIL_IS_NOT_TAKEN() {
        String[] loginData = Helper.getRandomLoginData();
        String[] loginData1 = Helper.getRandomLoginData();
        assertEquals(DBQueries.register(loginData[0], loginData[1]), true);
        assertEquals(DBQueries.register(loginData1[0], loginData1[1]), true);
    }
    
    @Test
    public void REGISTER_SHOULD_RETURN_FALSE_WHEN_EMAIL_IS_TAKEN() {
        String[] loginData = Helper.getRandomLoginData();
        assertEquals(DBQueries.register(loginData[0], loginData[1]), true);
        assertEquals(DBQueries.register(loginData[0], loginData[1]), false);
    }
    
    @Test
    public void IS_EXISTING_SHOULD_RETURN_FALSE_WHEN_DB_IS_EMPTY() {
        assertEquals(DBQueries.isExisting(WRONG_URL), false);
    }
    
    @Test
    public void IS_EXISTING_SHOULD_RETURN_FALSE_WHEN_URL_IS_NOT_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        String testUrl = UUID.randomUUID().toString();
        Helper.insertDataToGadgets(v_connection, loginData[0], testUrl);
        assertEquals(DBQueries.isExisting(WRONG_URL), false);
    }
    
    @Test
    public void IS_EXISTING_SHOULD_RETURN_TRUE_WHEN_URL_IS_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        String testUrl = UUID.randomUUID().toString();
        Helper.insertDataToGadgets(v_connection, loginData[0], testUrl);
        assertEquals(DBQueries.isExisting(testUrl), true);
    }
    
    @Test
    public void SAVE_GADGET_SHOULD_RETURN_TRUE_WHEN_URL_IS_NOT_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        DBQueries.email = dto.email;
        assertEquals(DBQueries.saveGadget(dto), true);
        assertEquals(Helper.count(v_connection), 1);
    }
    
    @Test
    public void SAVE_GADGET_SHOULD_RETURN_TRUE_WHEN_URL_IS_PRESENT_UPDATE() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        DBQueries.email = dto.email;
        assertEquals(DBQueries.saveGadget(dto), true);
        assertEquals(Helper.count(v_connection), 1);
        dto.beschreibung = "update";
        assertEquals(DBQueries.saveGadget(dto), true);
        assertEquals(Helper.count(v_connection), 1);
    }
    
    @Test
    public void DELETE_GADGET_SHOULD_RETURN_0_WHEN_DB_IS_EMPTY() {
        assertEquals(DBQueries.deleteGadget(WRONG_URL, WRONG_URL), 0);
        assertEquals(Helper.count(v_connection), 0);
    }
    
    @Test
    public void DELETE_GADGET_SHOULD_RETURN_0_WHEN_USER_EMAIL_IS_WRONG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(DBQueries.deleteGadget(dto.url, WRONG_URL), 0);
        assertEquals(Helper.count(v_connection), 1);
    }
    
    @Test
    public void DELETE_GADGET_SHOULD_RETURN_0_WHEN_GADGET_URL_IS_WRONG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(DBQueries.deleteGadget(WRONG_URL, loginData[0]), 0);
        assertEquals(Helper.count(v_connection), 1);
    }
    
    @Test
    public void DELETE_GADGET_SHOULD_RETURN_1_WHEN_GADGET_IS_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        DBQueries.email = dto.email;
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(Helper.count(v_connection), 1);
        assertEquals(DBQueries.deleteGadget(dto.url, loginData[0]), 1);
        assertEquals(Helper.count(v_connection), 0);
    }
    
    @Test
    public void DELETE_COMMENT_SHOULD_RETURN_0_WHEN_DB_IS_EMPTY() {
        assertEquals(DBQueries.deleteComment(WRONG_URL, WRONG_URL), 0);
    }
    
    @Test
    public void DELETE_COMMENT_SHOULD_RETURN_0_WHEN_USER_EMAIL_IS_WRONG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        Helper.addComment(v_connection, loginData[0], dto.url);
        assertEquals(DBQueries.deleteComment(WRONG_URL, dto.url), 0);
    }
    
    @Test
    public void DELETE_COMMENT_SHOULD_RETURN_0_WHEN_GADGET_URL_IS_WRONG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        Helper.addComment(v_connection, loginData[0], dto.url);
        assertEquals(DBQueries.deleteComment(loginData[0], WRONG_URL), 0);
    }
    
    @Test
    public void DELETE_COMMENT_SHOULD_RETURN_1_WHEN_GADGET_IS_PRESENT() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        Helper.addComment(v_connection, loginData[0], dto.url);
        assertEquals(DBQueries.deleteComment(loginData[0], dto.url), 0);
    }
    
    @Test
    public void READ_COMMENTS_SHOULD_RETURN_EMPTY_STRING_WHEN_DB_IS_EMPTY() {
        assertEquals(DBQueries.readComments(WRONG_URL), "");
    }
    
    @Test
    public void READ_COMMENTS_SHOULD_RETURN_EMPTY_STRING_WHEN_URL_IS_NOT_PRESENT_IN_COMMENTS() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        Helper.addComment(v_connection, loginData[0], dto.url);
        assertEquals(DBQueries.readComments(WRONG_URL), "");
    }
    
    @Test
    public void READ_COMMENTS_SHOULD_RETURN_CONCATENATED_STRING_WHEN_URL_IS_PRESENT_IN_COMMENTS() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        ArrayList<String> comments = new ArrayList<>();
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String[] loginData1 = Helper.getRandomLoginData();
            Helper.standardLogin(v_connection, loginData1[0], loginData1[1]);
            String comment = UUID.randomUUID().toString();
            comments.add(comment);
            Helper.addComment(v_connection, loginData1[0], dto.url, comment);
        });
        String result = DBQueries.readComments(dto.url);
        comments.forEach(c -> assertEquals(result.contains(c + "\n"), true));
    }
    
    @Test
    public void FIRST_SHOULD_RETURN_NULL_WHEN_NOT_LOGGED_IN() {
        assertEquals(DBQueries.first(), null);
    }
    
    @Test
    public void FIRST_SHOULD_RETURN_NULL_WHEN_GADGET_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.first(), null);
    }
    
    @Test
    public void FIRST_SHOULD_RETURN_FIRST_DTO_SORTED_ASCENDING() {
        DBQueries.logIn = true;
        ArrayList<String> urls = new ArrayList<>();
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String gadgetUrl = UUID.randomUUID().toString().replaceAll("[0-9]", "").replaceAll("-", "");
            urls.add(gadgetUrl);
            DTO dto = DTOHelper.generateDTO(loginData[0], gadgetUrl);
            Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        });
        Collections.sort(urls);
        assertEquals(urls.get(0), DBQueries.first().url);
    }
    
    @Test
    public void LAST_SHOULD_RETURN_NULL_WHEN_NOT_LOGGED_IN() {
        assertEquals(DBQueries.last(), null);
    }
    
    @Test
    public void LAST_SHOULD_RETURN_NULL_WHEN_GADGET_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.last(), null);
    }
    
    @Test
    public void LAST_SHOULD_RETURN_LAST_DTO_SORTED_ASCENDING() {
        DBQueries.logIn = true;
        ArrayList<String> urls = new ArrayList<>();
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String gadgetUrl = UUID.randomUUID().toString().replaceAll("[0-9]", "").replaceAll("-", "");
            urls.add(gadgetUrl);
            DTO dto = DTOHelper.generateDTO(loginData[0], gadgetUrl);
            Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        });
        Collections.sort(urls);
        assertEquals(urls.get(urls.size() - 1), DBQueries.last().url);
    }
    
    @Test
    public void NEXT_SHOULD_RETURN_NULL_WHEN_NOT_LOGGED_IN() {
        assertEquals(DBQueries.next(), null);
    }
    
    @Test
    public void NEXT_SHOULD_RETURN_NULL_WHEN_GADGET_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.next(), null);
    }
    
    @Test
    public void NEXT_SHOULD_RETURN_NEXT_DTO_SORTED_ASCENDING() {
        DBQueries.logIn = true;
        DBQueries.rsPointer = 0;
        ArrayList<String> urls = new ArrayList<>();
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String gadgetUrl = UUID.randomUUID().toString().replaceAll("[0-9]", "").replaceAll("-", "");
            urls.add(gadgetUrl);
            DTO dto = DTOHelper.generateDTO(loginData[0], gadgetUrl);
            Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        });
        
        Collections.sort(urls);
        urls.forEach(u -> assertEquals(u, DBQueries.next().url));
        assertEquals(urls.get(0), DBQueries.next().url);
    }
    
    @Test
    public void PREVIOUS_SHOULD_RETURN_NULL_WHEN_NOT_LOGGED_IN() {
        assertEquals(DBQueries.previous(), null);
    }
    
    @Test
    public void PREVIOUS_SHOULD_RETURN_NULL_WHEN_GADGET_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.previous(), null);
    }
    
    @Test
    public void PREVIOUS_SHOULD_RETURN_NEXT_DTO_SORTED_ASCENDING() {
        DBQueries.logIn = true;
        DBQueries.rsPointer = 0;
        ArrayList<String> urls = new ArrayList<>();
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String gadgetUrl = UUID.randomUUID().toString().replaceAll("[0-9]", "").replaceAll("-", "");
            urls.add(gadgetUrl);
            DTO dto = DTOHelper.generateDTO(loginData[0], gadgetUrl);
            Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        });
        
        Collections.sort(urls);
        Collections.reverse(urls);
        urls.forEach(u -> assertEquals(u, DBQueries.previous().url));
        assertEquals(urls.get(0), DBQueries.previous().url);
    }
    
    @Test
    public void READ_AT_ROW_SHOULD_RETURN_NULL_WHEN_NOT_LOGGED_IN() {
        assertEquals(DBQueries.readAtRow(new Random().nextInt(1000) + 1), null);
    }
    
    @Test
    public void READ_AT_ROW_SHOULD_RETURN_NULL_WHEN_GADGET_IS_EMPTY() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        assertEquals(DBQueries.readAtRow(new Random().nextInt(1000) + 1), null);
    }
    
    @Test
    public void READ_AT_ROW_SHOULD_RETURN_NEXT_DTO_SORTED_ASCENDING() {
        DBQueries.logIn = true;
        ArrayList<String> urls = new ArrayList<>();
        ArrayList<Integer> nums = new ArrayList<>();
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        int count = new Random().nextInt(1000) + 1;
        IntStream.range(0, count).boxed().forEach(i -> {
            String gadgetUrl = UUID.randomUUID().toString().replaceAll("[0-9]", "").replaceAll("-", "");
            urls.add(gadgetUrl);
            nums.add(i);
            DTO dto = DTOHelper.generateDTO(loginData[0], gadgetUrl);
            Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        });
        
        Collections.sort(urls);
        Collections.shuffle(nums);
        nums.forEach(i -> assertEquals(urls.get(i), DBQueries.readAtRow(i + 1).url));
    }
    
    @Test
    public void AVG_BEWERTUNG_SHOULD_RETURN_0_WHEN_GADGET_URL_DOES_NOT_EXIST() {
        assertEquals(DBQueries.avgBewertung(WRONG_URL), 0.0);
    }
    
    @Test
    public void AVG_BEWERTUNG_SHOULD_RETURN_0_WHEN_GADGET_HAS_NO_BEWERTUNG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(DBQueries.avgBewertung(WRONG_URL), 0.0);
    }
    
    @Test
    public void AVG_BEWERTUNG_SHOULD_RETURN_AVG_WHEN_GADGET_HAS_BEWERTUNG() {
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        
        ArrayList<Integer> bewertungen = new ArrayList<>();
        int count = new Random().nextInt(1000) + 1;
        
        IntStream.range(0, count).boxed().forEach(i -> {
            int bewertung = new Random().nextInt(11);
            bewertungen.add(bewertung);
            String[] loginData1 = Helper.getRandomLoginData();
            Helper.standardLogin(v_connection, loginData1[0], loginData1[1]);
            Helper.addBewertung(v_connection, loginData1[0], dto.url, bewertung);
        });
        
        assertEquals(DBQueries.avgBewertung(dto.url), bewertungen.stream().mapToInt(i -> i).average().getAsDouble());
    }
    
    @Test
    public void UPDATE_COMMENT_SHOULD_RETURN_0_WHEN_DB_IS_EMPTY() {
        String comment = UUID.randomUUID().toString();
        int num = new Random().nextInt(11);
        assertEquals(DBQueries.updateComment(WRONG_URL, WRONG_URL, comment, num), 0);
    }
    
    @Test
    public void UPDATE_COMMENT_SHOULD_RETURN_0_WHEN_GADGET_URL_IS_WRONG() {
        String comment = UUID.randomUUID().toString();
        int num = new Random().nextInt(11);
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(DBQueries.updateComment(WRONG_EMAIL, WRONG_EMAIL, comment, num), 0);
    }
    
    @Test
    public void UPDATE_COMMENT_SHOULD_RETURN_1_WHEN_GADGET_URL_IS_RIGHT() {
        String comment = UUID.randomUUID().toString();
        int num = new Random().nextInt(11);
        String[] loginData = Helper.getRandomLoginData();
        Helper.standardLogin(v_connection, loginData[0], loginData[1]);
        DTO dto = DTOHelper.generateDTO(loginData[0]);
        Helper.insertDataToGadgets(v_connection, dto.url, dto.email, dto.beschreibung);
        assertEquals(DBQueries.updateComment(loginData[0], dto.url, comment, num), 1);
        assertEquals(DBQueries.updateComment(loginData[0], dto.url, comment, num), 1);
    }
}
