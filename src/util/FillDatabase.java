package util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import java.sql.*;
import javax.imageio.ImageIO;

/**
 *
 * @author student
 */
public class FillDatabase {
    
    public static String url = "jdbc:oracle:thin:@//localhost:1521/xe";
    
    public static String FILE_PATH = "../";

    public static void main(String[] args) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection v_connection = DriverManager.getConnection(url,
                    "CHRIS",
                    "xyz"
            );
            CsvUSERSToDb(v_connection);
            System.out.println("copy users");
            CsvGADGETSToDb(v_connection);
            System.out.println("copy gadgets");
            CsvBEWERTUNGToDb(v_connection);
            System.out.println("copy bewertungen");
            v_connection.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void CsvUSERSToDb(Connection v_connection) {
        String users = "insert into USERS values (?, ?)";

        try ( BufferedReader br = new BufferedReader(new FileReader(new File("resources/USERS.csv")));  PreparedStatement ps = v_connection.prepareStatement(users)) {
            br.lines().map(s -> s.split("§\\$%&"))
                    .forEach(array -> {
                        try {
                            ps.setString(1, array[0]);
                            ps.setString(2, array[1]);
                            ps.execute();
                        } catch (SQLException ex) {
                            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
        } catch (Exception ex) {
            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void CsvGADGETSToDb(Connection v_connection) {
        String gadgets = "insert into GADGETS values (?, ?, ?, ?)";

        try ( BufferedReader br = new BufferedReader(new FileReader(new File("resources/GADGETS.csv")));  PreparedStatement ps = v_connection.prepareStatement(gadgets)) {
            br.lines().map(s -> s.split("§\\$%&"))
                    .forEach(array -> {
                        try {
                            ps.setString(1, array[0].equals("@null@") ? null : array[0].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.setString(2, array[1].equals("@null@") ? null : array[1].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.setString(3, array[2].equals("@null@") ? null : array[2].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.setBlob(4, array[3].equals("@null@") ? null : ImageIcon2Blob(new ImageIcon(Base64.getDecoder().decode(array[3])), v_connection));
                            ps.execute();
                        } catch (SQLException ex) {
                            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
        } catch (Exception ex) {
            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void CsvBEWERTUNGToDb(Connection v_connection) {
        String bewertung = "insert into BEWERTUNG values (?, ?, ?, ?)";

        try ( BufferedReader br = new BufferedReader(new FileReader(new File("resources/BEWERTUNG.csv")));  PreparedStatement ps = v_connection.prepareStatement(bewertung)) {
            br.lines().map(s -> s.split("§\\$%&"))
                    .forEach(array -> {
                        try {
                            ps.setString(1, array[0].equals("@null@") ? null : array[0].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.setString(2, array[1].equals("@null@") ? null : array[1].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.setInt(3, array[2].equals("@null@") ? null : Integer.valueOf(array[2]));
                            ps.setString(4, array[3].equals("@null@") ? null : array[3].replaceAll("(@newline@)+", System.lineSeparator()));
                            ps.execute();
                        } catch (SQLException ex) {
                            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
        } catch (Exception ex) {
            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Blob ImageIcon2Blob(ImageIcon myImageIcon, Connection con) {
        if (myImageIcon == null)  return null;
        Blob myBlob = null;
         
        try {
           
            ByteArrayOutputStream myByteArrayOutputStream = new ByteArrayOutputStream();
            BufferedImage buImg = new BufferedImage(myImageIcon.getIconWidth(), myImageIcon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            buImg.getGraphics().drawImage(myImageIcon.getImage(), 0, 0, myImageIcon.getImageObserver());
            ImageIO.write(buImg, "gif", myByteArrayOutputStream);
            byte[] myByteArray = myByteArrayOutputStream.toByteArray();
            myBlob =  con.createBlob();
            myBlob.setBytes(1, myByteArray);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(FillDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myBlob;
    }

}
