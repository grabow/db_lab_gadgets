/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edu.hsog;

import org.apache.commons.dbcp2.BasicDataSource;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class Globals {

    static String url = "jdbc:oracle:thin:@//localhost:1521/xe";
    static String username = "chris";
    static String passwd = "xyz";

    static BasicDataSource conPool = null;

    public static void initConnectionPool() {

        System.out.println("Account: " + username + ":" + passwd);

        conPool = new BasicDataSource();
        conPool.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        conPool.setUrl(url);
        conPool.setUsername(username);
        conPool.setPassword(passwd);
        conPool.setMaxTotal(15);
        conPool.setInitialSize(5);
    }

    public static Connection getPoolConnection() {
        Connection v_connection = null;
        try {
            v_connection = conPool.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(Globals.class.getName()).log(Level.SEVERE, null, ex);
        }
        return v_connection;
    }

}
