
package edu.hsog;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBQueries {
    
    static boolean logIn = false;    // Logged in or not
    static String email = null;      // eMail of Usefrt
    static int rsPointer = 1;        // Points to current dataset in resultset
    
    public static int count() {
        return -1;
    }
    
    static public boolean login(String email, String passwd) {
        return false;
    }

    static public boolean register(String email, String passwd) {
        return false;
    }

    public static boolean isExisting(String url) {
        return false;
    }

    public static boolean saveGadget(DTO dto) {
        return false;
    }

    static public int deleteGadget(String url, String eMail) {
        return 0;
    }
    
    static public int deleteComment(String url, String eMail) {
        return 0;
    }
    
    static public String readComments(String url) {
        return "";
    }
    
    static public DTO first() {
        return null;
    }

    static public DTO last() {
        return null;
    }

    static public DTO next() {
        return null;
    }

    static public DTO previous() {
        return null;
    }

    static public DTO readAtRow(int num) {
        return null;
    }

    static public double avgBewertung(String url) {
        return 0.0;
    }
    
    static public int updateComment(String email, String url, String kommentar, int gefallen) {
        return 0;
    }
}
